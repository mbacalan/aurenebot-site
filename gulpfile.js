const gulp = require("gulp");
const browserSync = require("browser-sync").create();
const sass = require("gulp-sass");
const prefixer = require("gulp-autoprefixer");
const uglify = require("gulp-uglify");
const concat = require("gulp-concat");
const imagemin = require("gulp-imagemin");

// DEVELOPMENT

//Transpile SCSS & add prefixes
function styles() {
  return gulp.src("src/scss/**/*.scss")
    .pipe(sass()).on("error", sass.logError)
    .pipe(prefixer({ browsers: ["last 2 versions"] }))
    .pipe(gulp.dest("src/css"))
    .pipe(browserSync.stream());
}

//Concatenate JS files
// function scripts() {
//   return gulp.src("src/js/**/main.js")
//     .pipe(concat("app.js"))
//     .pipe(gulp.dest("src/js"));
// }

//Initialize browserSync
function watch() {
  browserSync.init({
    proxy: "http://localhost:3000",
    // server: {
    //   baseDir: "./src"
    // }
  });
  gulp.watch(["src/*.html", "src/js/*.js"]).on("change", browserSync.reload);
  gulp.watch("src/scss/**/*.scss", styles);
  // gulp.watch("src/js/*.js", scripts);
}

// PRODUCTION

//Copy files to dist/
function copyHtml() {
  return gulp.src("src/index.html")
    .pipe(gulp.dest("dist/"));
}

function imageDist() {
  return gulp.src("src/img/*")
    .pipe(imagemin())
    .pipe(gulp.dest("dist/img"));
}

//Minify CSS for production
function stylesDist() {
  return gulp.src("src/scss/**/*.scss")
    .pipe(sass({ outputStyle: "compressed" })).on("error", sass.logError)
    .pipe(prefixer({ browsers: ["last 2 versions"] }))
    .pipe(gulp.dest("dist/css"));
}

//Minify and concatenate JS for production
function scriptsDist() {
  return gulp.src("src/js/**/main.js")
    .pipe(uglify())
    .pipe(concat("main.js"))
    .pipe(gulp.dest("dist/js"));
}

exports.default = watch;
exports.dist = gulp.series(copyHtml, imageDist, stylesDist, scriptsDist);
