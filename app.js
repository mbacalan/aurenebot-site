const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const app = express();
// Allows using local .env file
require("dotenv").config();

// Use middleware to create JSON from submitted data
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

// Specifiy static files directories for Express to use
app.use(express.static("src"));
app.use(express.static("dist"));

// Connect to mongoDB
mongoose.connect(process.env.MONGODB_URI || "mongodb://localhost:27017/local", ({
  useNewUrlParser: true,
}));

// Provide feedback regarding the connection status
mongoose.connection.on("error", console.error.bind(console, "connection error:"));
mongoose.connection.once("open", () => console.log("Succesfully connected to database"));

// Define schema for mongoDB
const quizSchema = new mongoose.Schema({
  question: String,
  answers: String,
  answer: String,
});

// Define a model using the schema
const Quiz = mongoose.model("Quiz", quizSchema);

// Return index.html at base URL
app.get("/", (req, res) => {
  res.sendFile(__dirname + "/src/index.html");
});

// Route HTTP POST requests
app.post("/addquiz", (req, res) => {
  // Create a new Quiz from requests body (gathered via body-parser)
  const myData = new Quiz(req.body);
  // Save the data to mongoDB
  myData.save()
    .then(() => {
      res.send("Quiz saved to database");
    })
    .catch(() => {
      res.status(400).send("Unable to save to database");
    });
});

// Listen for connections on given port
app.listen(process.env.PORT, () => {
  console.log(`Server listening on port ${process.env.PORT}`);
});
